const gulp = require('gulp');
const eslint = require('gulp-eslint');

gulp.task('lint', () =>
	gulp.src(['**/*.js', '!node_modules/**', '!native-base-theme/**'])
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError()));
