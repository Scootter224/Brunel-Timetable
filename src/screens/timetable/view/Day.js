import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardItem, Text } from 'native-base';
import { Agenda } from 'react-native-calendars';
import ScreenContainer from '../../../components/ScreenContainer';
import ItemCard from '../../../components/ItemCard';

function renderItem(item) {
	return (
		<ItemCard item={item} />
	);
}

function rowHasChanged(r1, r2) {
	return r1.name !== r2.name;
}

function timeToString(time) {
	const date = new Date(time);
	return date.toISOString().split('T')[0];
}

function today() {
	const day = new Date();
	let dd = day.getDate();
	let mm = day.getMonth() + 1;
	const yyyy = day.getFullYear();

	if (dd < 10) {
		dd = `0${dd}`;
	}

	if (mm < 10) {
		mm = `0${mm}`;
	}

	return '2018-01-01';
	// return `${yyyy}-${mm}-${dd}`;
}

function renderEmptyDate() {
	return (
		<Card>
			<CardItem header>
				<Text>Empty</Text>
			</CardItem>
		</Card>
	);
}

function formatNum(num) {
	if (num < 10) {
		return `0${num}`;
	}
	return `${num}`;
}

export default class Day extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			items: {},
			selected: today(),
			timetable: null,
			dataLoaded: false,
		};

		this.loadItems = this.loadItems.bind(this);
	}

	componentWillMount() {
		global.storage.load({
			key: 'Timetable',
		}).then((timetable) => {
			this.setState({ ...this.state, timetable, dataLoaded: true });
		}).catch((err) => {
			console.warn(err.message);
			switch (err.name) {
				case 'NotFoundError':
					this.props.navigation.navigate('Login');
					break;
				case 'ExpiredError':
					this.props.navigation.navigate('Login');
					break;
				default:
				// TODO
			}
		});
	}

	loadItems(day) {
		setTimeout(() => {
			let month = this.state.timetable[day.year][day.month];

			if (month === undefined) {
				month = {};
			}

			for (let i = 1; i <= 32; i += 1) {
				if (month[i] === undefined) {
					this.state.items[`${day.year}-${formatNum(day.month)}-${formatNum(i)}`] = [];
				} else {
					this.state.items[`${day.year}-${formatNum(day.month)}-${formatNum(i)}`] = month[i];
				}
			}

			const newItems = {};
			Object.keys(this.state.items).forEach((key) => { newItems[key] = this.state.items[key]; });
			this.setState({
				items: newItems,
			});
		}, 1000);
		console.log(`Load Items for ${day.year}-${day.month}`);
		console.log(`Items ${JSON.stringify(this.state.items, null, 4)}`);
	}

	render() {
		return (
			<ScreenContainer title="Day" navigation={this.props.navigation} noContent>
				{this.state.dataLoaded && <Agenda
					items={this.state.items}
					loadItemsForMonth={this.loadItems}
					selected={this.state.selected}
					renderItem={renderItem}
					renderEmptyDate={renderEmptyDate}
					rowHasChanged={rowHasChanged}
					theme={{
						backgroundColor: '#ffffff',
						calendarBackground: '#ffffff',
						textSectionTitleColor: '#b6c1cd',
						selectedDayBackgroundColor: '#0F2645',
						selectedDayTextColor: '#ffffff',
						todayTextColor: '#0F2645',
						dayTextColor: '#2d4150',
						textDisabledColor: '#d9e1e8',
						dotColor: '#0F2645',
						selectedDotColor: '#ffffff',
						arrowColor: '#00325b',
						monthTextColor: '#0F2645',
						agendaDayTextColor: '#0F2645',
						agendaDayNumColor: '#0F2645',
						agendaTodayColor: '#C01422',
						agendaKnobColor: '#00325b',
					}}
				/>}
			</ScreenContainer>
		);
	}
}

Day.propTypes = {
	navigation: PropTypes.any,
};
