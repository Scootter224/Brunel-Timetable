import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { H1, Body, Form, Picker, Input, Item, Icon, Header, Button, Text, Label } from 'native-base';
import Fuse from 'fuse.js';
import { UnsafeHash } from '../../helpers/utils';
import ScreenContainer from '../../components/ScreenContainer';
import WebView from '../../components/WebView';

const { Platform } = require('react-native');

function renderLevelItems(list) {
	for (let i = 0; i < list.length; i += 1) {
		if (list[i].label === 'Please select level') {
			list.splice(i, 1);
		}
	}

	list.sort((a, b) => a.label > b.label);
	const elements = list.map(data => (<Picker.Item
		label={data.label}
		value={data.value}
		key={UnsafeHash(data.label + data.value)}
	/>));
	if (Platform.OS === 'android') {
		elements.unshift(<Picker.Item label="Select" />);
	}
	return elements;
}

export default class Get extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			course: undefined,
			level: undefined,
			courses: [],
			levels: [],
			searchTerm: '',
			searchText: '',
		};

		this.onValueChangeCourse = this.onValueChangeCourse.bind(this);
		this.onValueChangeLevel = this.onValueChangeLevel.bind(this);
		this.onLoad = this.onLoad.bind(this);
		this.gotPageTitle = this.gotPageTitle.bind(this);
		this.gotCourseList = this.gotCourseList.bind(this);
		this.gotLevelList = this.gotLevelList.bind(this);
		this.renderCourseItems = this.renderCourseItems.bind(this);
		this.submit = this.submit.bind(this);
	}

	onLoad(url) {
		if (url.includes('showtimetable.aspx')) {
			this.props.navigation.navigate('Process');
		}
		this.webView.getPageTitle();
	}

	onValueChangeCourse(value) {
		this.setState({
			...this.state,
			course: value,
		});
	}

	onValueChangeLevel(value) {
		this.setState({
			...this.state,
			level: value,
		});
		this.webView.setLevel(value);
	}

	gotPageTitle(title) {
		if (title !== 'Course Timetables') {
			this.webView.clickElement('LinkBtn_pos');
		} else {
			this.webView.getCourseList();
			this.webView.getLevelList();
		}
	}

	gotCourseList(list) {
		this.setState({ ...this.state, courses: list });
	}

	gotLevelList(list) {
		this.setState({ ...this.state, levels: list });
	}

	submit() {
		this.webView.setForm(this.state.level, this.state.course);
	}

	renderCourseItems() {
		const options = {
			shouldSort: true,
			threshold: 0.6,
			location: 0,
			distance: 100,
			maxPatternLength: 32,
			minMatchCharLength: 1,
			tokenize: true,
			keys: [
				'label',
			],
		};
		const fuse = new Fuse(this.state.courses, options);
		const result = fuse.search(`${this.state.searchTerm}`);

		const elements = result.map(data => (<Picker.Item
			label={data.label}
			value={data.value}
			key={UnsafeHash(data.label + data.value)}
		/>));
		if (Platform.OS === 'android') {
			elements.unshift(<Picker.Item label="Select" />);
		}
		return elements;
	}

	render() {
		return (
			<ScreenContainer title="Select Course" navigation={this.props.navigation} padder logo>
				<Body>
					<H1>Select Course</H1>
				</Body>
				<Form>
					<Item
						error={this.state.idError}
						success={this.state.idSuccess}
						style={{ marginLeft: 0 }}
						stackedLabel
					>
						<Label>Level</Label>
						<Picker
							mode="dropdown"
							placeholder="Select"
							selectedValue={this.state.level}
							onValueChange={this.onValueChangeLevel}
							iosHeader="Select Level"
						>
							{renderLevelItems(this.state.levels)}
						</Picker>
					</Item>
					<Item
						error={this.state.idError}
						success={this.state.idSuccess}
						style={{ marginLeft: 0 }}
						stackedLabel
					>
						<Label>Course</Label>
						<Picker
							mode="dropdown"
							placeholder="Select"
							selectedValue={this.state.course}
							onValueChange={this.onValueChangeCourse}
							iosHeader="Select Course"
							renderHeader={backAction =>
								(
									<Header searchBar rounded style={{ paddingLeft: 2 }}>
										<Button transparent onPress={backAction} style={{ paddingLeft: 0 }}>
											<Icon name="ios-arrow-back" />
										</Button>
										<Item style={{ backgroundColor: '#fff' }} >
											<Icon name="ios-search" />
											<Input
												autoFocus
												placeholder="Search"
												value={this.state.searchText}
												onChangeText={text => this.setState({ ...this.state, searchText: text })}
												onSubmitEditing={() => this.setState({
													...this.state,
													searchTerm: this.state.searchText,
												})
												}
												returnKeyType="search"
											/>
											<Icon
												name="ios-close-circle"
												onPress={() => this.setState({
													...this.state,
													searchTerm: '',
													searchText: '',
												})}
											/>
										</Item>
									</Header>
								)}
						>
							{this.renderCourseItems()}
						</Picker>
					</Item>
				</Form>
				<View style={{ height: 25 }} />
				<Button block onPress={() => this.submit()}>
					<Text>Go!</Text>
				</Button>
				<WebView
					url="https://teaching.brunel.ac.uk/SWS-1718/default.aspx"
					ref={webView => this.webView = webView}
					onLoad={this.onLoad}
					getPageTitleCallback={this.gotPageTitle}
					getCourseListCallback={this.gotCourseList}
					getLevelListCallback={this.gotLevelList}
				/>
			</ScreenContainer>
		);
	}
}

Get.propTypes = {
	navigation: PropTypes.any,
};
