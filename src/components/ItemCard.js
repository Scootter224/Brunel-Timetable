import React from 'react';
import PropTypes from 'prop-types';
import { Modal, TouchableHighlight } from 'react-native';
import { Body, Card, CardItem, Text, Button, View, H1, Label } from 'native-base';

const { Dimensions } = require('react-native');

const deviceHeight = Dimensions.get('window').height;

export default class ItemCard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			modalShow: false,
		};

		this.toggleModal = this.toggleModal.bind(this);
	}

	toggleModal() {
		this.setState({ ...this.state, modalShow: !this.state.modalShow });
	}

	names() {
		const { staff } = this.props.item;
		if (!staff) {
			return 'No staff';
		}
		let output = '';
		Object.keys(staff).forEach(key => output += `${staff[key].firstname.trim()} ${staff[key].surname.trim()} ${key < staff.length - 1 ? ', ' : ''}`);
		return output;
	}

	weeks() {
		const { weeks } = this.props.item;
		if (!weeks) {
			return 'No weeks';
		}
		let output = '';
		Object.keys(weeks).forEach(key => output += `${weeks[key].start.num} - ${weeks[key].end.num} ${key < weeks.length - 1 ? ', ' : ''}`);
		return output;
	}

	render() {
		const { item } = this.props;
		return (
			<TouchableHighlight onPress={this.toggleModal} underlayColor="#fff">
				<Card>
					<Modal
						animationType="fade"
						transparent
						visible={this.state.modalShow}
						presentationStyle="overFullScreen"
					>
						<View style={{ paddingTop: deviceHeight / 4, flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
							<Card style={{ flex: 0.43, paddingBottom: 0 }}>
								<CardItem header>
									<H1 style={{ textAlign: 'center', width: '100%' }}>{item.activity}</H1>
								</CardItem>
								<CardItem>
									<Body>
										<Text style={{ textAlign: 'center' }}><Label style={{ fontWeight: '600' }}>Activity:</Label> {item.activity} </Text>
										<Text style={{ textAlign: 'center' }}><Label style={{ fontWeight: '600' }}>Description:</Label> {item.description} </Text>
										<Text style={{ textAlign: 'center' }}><Label style={{ fontWeight: '600' }}>Room:</Label> {item.room} </Text>
										<Text style={{ textAlign: 'center' }}><Label style={{ fontWeight: '600' }}>Names:</Label> {this.names()}</Text>
										<Text style={{ textAlign: 'center' }}><Label style={{ fontWeight: '600' }}>Start:</Label> {item.start} - {item.end} </Text>
										<Text style={{ textAlign: 'center' }}><Label style={{ fontWeight: '600' }}>Type:</Label> {item.type} </Text>
										<Text style={{ textAlign: 'center' }}><Label style={{ fontWeight: '600' }}>Weeks:</Label> {this.weeks()} </Text>


									</Body>
								</CardItem>
								<CardItem>
									<Button block style={{ width: '100%' }} onPress={this.toggleModal}>
										<Text>Close</Text>
									</Button>
								</CardItem>
							</Card>
						</View>
					</Modal>
					<CardItem header>
						<Text>{item.activity}</Text>
					</CardItem>
					<CardItem>
						<Body>
							<Text>
								{item.start} - {item.end}  |  {item.room}
							</Text>
						</Body>
					</CardItem>
				</Card>
			</TouchableHighlight>
		);
	}
}

ItemCard.propTypes = {
	item: PropTypes.object,
};
