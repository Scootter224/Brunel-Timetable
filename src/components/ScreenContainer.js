import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import { Body, Title, Container, Content, Header, Icon, Left, Button, Right } from 'native-base';

const { Dimensions } = require('react-native');

const deviceHeight = Dimensions.get('window').height;

const logo = require('../images/logo.png');
const image = require('../images/brunel.jpg');

const styles = {
	container: {
		backgroundColor: '#FFF',
		flex: 1,
		paddingBottom: 25,
	},
	logo: {
		resizeMode: 'contain',
		height: deviceHeight / 12,
		width: null,
		position: 'relative',
		marginBottom: 25,
		marginTop: 25,
	},

	image: {
		alignSelf: 'stretch',
		// height: deviceHeight / 2,
		width: null,
		position: 'relative',
		marginBottom: 0,
		bottom: 0,
		flex: 1,
	},
};

const ScreenContainer = props => (
	<Container style={styles.container}>
		<Header>
			<Left>
				<Button
					transparent
					onPress={props.navigation.openDrawer}
				>
					<Icon name="menu" />
				</Button>
			</Left>
			<Body>
				<Title>{props.title}</Title>
			</Body>
			<Right />
		</Header>
		{!props.noContent &&
			<Content padder={props.padder}>
				{props.logo && <Image square style={styles.logo} source={logo} />}
				{props.children}
			</Content>}
		{props.noContent && props.children}
		{props.image && <Image source={image} style={styles.image} />}
	</Container>
);

ScreenContainer.propTypes = {
	title: PropTypes.string,
	padder: PropTypes.bool,
	logo: PropTypes.bool,
	image: PropTypes.bool,
	children: PropTypes.any,
	noContent: PropTypes.bool,
	navigation: PropTypes.any,
};

export default ScreenContainer;
